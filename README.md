# Webform Anonymizer

This simple module adds a checkbox to the bottom of Settings>Form for a particular form built using Webform. 

By default, the "Confidential submissions" setting in Webform requires users to be logged out. This module can also allow anonymous submissions from users who are currently logged into the Drupal site. 

It works by deleting the UID and the IP Address when a user submits a form. 